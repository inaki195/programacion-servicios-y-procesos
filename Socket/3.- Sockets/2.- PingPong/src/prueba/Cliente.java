package prueba;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class Cliente {
    private int puerto;
    private String host;
    private Socket socket;

    public Cliente(String localhost, int i) {
        this.puerto = puerto;
        this.host = host;
    }

    public static void main(String[] args) {
        Cliente c = new Cliente("localhost",4444);
        try {
            c.conectar();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void conectar() throws IOException {
        socket = new Socket(host, puerto);

        // Declaramos un DataInputStream para leer los datos
        DataInputStream entrada = new DataInputStream(socket.getInputStream());

        // Declaramos un DataOutputStream para escribir los datos
        PrintStream salida = new PrintStream(socket.getOutputStream());

        // Creamos un String para la informacion recibida
        String recibida;

        salida.println("Pong");
        recibida = entrada.readLine();
        System.out.println("Soy el cliente, y he recibido un "+recibida);

        //Se cierra en canal de entrada
        entrada.close();
        //Se cierra el canal de salida
        salida.close();
        //Se cierra el socket
        socket.close();
    }
}
