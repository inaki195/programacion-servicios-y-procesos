package prueba;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {

        private ServerSocket socketServidor;
        private int puerto;
    public Server(int puerto) {
        this.puerto = puerto;
        //Se inicia el servidor
        arrancarServidor();
    }

    private void arrancarServidor() {
        //control de errores
        try {
            //Se crea el ServerSocket
            socketServidor = new ServerSocket(this.puerto);

            /* Se crea el socket de conexión del cliente para trabajar con él.
               En un caso más real se utilizaría para crear los canales de
               comunicacion de entrada y salida con la aplicación cliente
            */
            Socket cienteConectado = socketServidor.accept();

            //Declaramos un PrintStream para escribir datos
            PrintStream salida = new PrintStream(cienteConectado.getOutputStream());

            // Creamos un String recibido para cuando recibamos la orden del cliente
            String recibido="";

            //Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(cienteConectado.getInputStream());

            recibido = entrada.readLine();
            System.out.println("Soy el servidor, he recibido un "+recibido);
            salida.println("Pong");

            //Se cierra el canal de entrada
            entrada.close();
            //Se cierra el canal de salida
            salida.close();
            //Se cierra el socket
            cienteConectado.close();

        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public static void main(String[] args) {

    }
}
