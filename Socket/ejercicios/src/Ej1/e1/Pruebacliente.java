package Ej1.e1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Pruebacliente {
    public static void main(String[] args) {
        //host
        final  String host="127.0.0.1";
        //puerto  que elijo
        final  int puerto=5003;
        try {
            Socket sc=new Socket(host,puerto);
            DataInputStream in = new DataInputStream(sc.getInputStream());
            //servidor al cliente manda datos
            DataOutputStream out = new DataOutputStream(sc.getOutputStream());
            //mensaje que envio al servidor
            out.writeUTF("ping ");
            //imprimo por consola
            System.out.println(in.readUTF());
            //cierro la conexion
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
