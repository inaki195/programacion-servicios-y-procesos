package Ej1.e2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente {
    Socket sc;
    public static void main(String[] args) {
        //host
        final String host = "127.0.0.1";
        //puerto  que elijo
        final int puerto = 5003;
        try {

            while (true) {
                Socket sc = new Socket(host, puerto);
                DataInputStream in = new DataInputStream(sc.getInputStream());
                //servidor al cliente manda datos
                DataOutputStream out = new DataOutputStream(sc.getOutputStream());
                //mensaje que envio al servidor
                out.writeUTF("ping ");
                //imprimo por consola el msg que recibo del server
                System.out.println(in.readUTF());
                sc.close();
            }
            //cierro la conexion

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
