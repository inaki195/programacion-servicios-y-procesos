package Ej1.e2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    public static void main(String[] args) {
        //la entrada es lo que recibe
        ServerSocket servidor=null;
        Socket sc=null;
        //cliente al servidor manda datos
        DataInputStream in;
        //servidor al cliente manda datos
        DataOutputStream out;
        try {


            //acepto conexion (se queda esperando hasta recibir una noticia)
            servidor=new ServerSocket(5003);
            //bucle infinito envia msg al cliente
            while (true) {

                sc =servidor.accept();
                in = new DataInputStream(sc.getInputStream());
                out = new DataOutputStream(sc.getOutputStream());
                //dato que recibo del cliente
                String msg = in.readUTF();
                //imprimo  por consola lo que he recibido del cliente
                System.out.println(msg);
                //msg que envio al cliente
                out.writeUTF("pong");
                //cierro conexion
                sc.close();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
