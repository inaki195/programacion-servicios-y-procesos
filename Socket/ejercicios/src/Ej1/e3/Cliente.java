package Ej1.e3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.BindException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente {
    Socket sc;
    public static void main(String[] args) {
        //host
        final String host = "127.0.0.1";
        //puerto  que elijo
        final int puerto = 5005;
        try {

            while (true) {
                Socket sc = new Socket(host, puerto);
                DataInputStream in = new DataInputStream(sc.getInputStream());
                //servidor al cliente manda datos
                DataOutputStream out = new DataOutputStream(sc.getOutputStream());
                //mensaje que envio al servidor
                out.writeUTF("ping ");
                //imprimo por consola

                sc.close();
            }
            //cierro la conexion

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (BindException e3){
            System.out.println("la direccion ya esta en uso");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
