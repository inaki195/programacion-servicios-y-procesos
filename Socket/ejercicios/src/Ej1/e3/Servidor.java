package Ej1.e3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {


    public static void main(String[] args) {
        int pines=0;
        //la entrada es lo que recibe
        ServerSocket servidor=null;
        Socket sc=null;
        //cliente al servidor manda datos
        DataInputStream in;
        //servidor al cliente manda datos
        DataOutputStream out;
        try {
            //acepto conexion (se queda esperando hasta recibir una noticia)
            servidor=new ServerSocket(5005);
            //bucle infinito envia msg al cliente
            while (true) {
                sc = servidor.accept();
                //cuando acepte conexion lo sumamos al total
                if (sc.isConnected()) {
                    pines++;
                }
                in = new DataInputStream(sc.getInputStream());
                //leo el msg y compruebo si es igual en caso afirmativo aumento el contador
                String msg = in.readUTF();
                System.out.println(msg);


                //cierro conexion
                sc.close();

                System.out.print("nº de conexiones " + pines);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
