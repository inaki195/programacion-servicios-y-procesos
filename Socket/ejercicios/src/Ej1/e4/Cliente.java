package Ej1.e4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.BindException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {
    Socket sc;
    public static void main(String[] args) {
        //host
        final String host = "127.0.0.1";
        //puerto  que elijo
        final int puerto = 5005;
        try {
            //declaro objeto scanner para coger datos por teclado
            Scanner in=new Scanner(System.in);
                Socket sc = new Socket(host, puerto);
                DataInputStream input = new DataInputStream(sc.getInputStream());
                //manda datos del cliente al servidor
                DataOutputStream out = new DataOutputStream(sc.getOutputStream());
                //pido 2 veces por teclado un numero
                //y lo envio al servidor
            System.out.println("n1");
            int n1=in.nextInt();
                out.writeByte(n1);
            System.out.println("n2");
            int n2=in.nextInt();
                 out.writeByte(n2);
                //imprimo por consola
            System.out.println(input.readInt());
                sc.close();

            //cierro la conexion

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (BindException e3){
            System.out.println("la direccion ya esta en uso");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
