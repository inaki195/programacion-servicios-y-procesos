package Ej1.e4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    public static void main(String[] args) {
        ServerSocket servidor=null;
        Socket sc=null;
        //cliente al servidor manda datos
        DataInputStream in;
        //servidor al cliente manda datos
        DataOutputStream out = null;
        try {
            //acepto conexion (se queda esperando hasta recibir una noticia)
            servidor=new ServerSocket(5005);

                //acepto solicitud
                sc = servidor.accept();

                out=new DataOutputStream(sc.getOutputStream());
                in = new DataInputStream(sc.getInputStream());
                //n1 n2 numeros que se reciben desde el servidor
                int n1 = in.read();
                int n2 = in.read();
                 int ntotal=n1+n2;
                System.out.println(" datos recibidos "+n1+" "+n2);
                //envio el resultado al cliente
                out.writeInt(ntotal);

                //cierro conexion
                sc.close();



        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
