package Ej1.e5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.BindException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {
    Socket sc;
    public static void main(String[] args) {
        //host
        final String host = "127.0.0.1";
        //puerto  que elijo
        final int puerto = 5005;
        try {
            String operacion="";
            //declaro objeto scanner para coger datos por teclado
            Scanner in=new Scanner(System.in);
                Socket sc = new Socket(host, puerto);
                DataInputStream input = new DataInputStream(sc.getInputStream());
                //manda datos del cliente al servidor
                DataOutputStream out = new DataOutputStream(sc.getOutputStream());
            System.out.println("operacion (unicos valores aceptados :suma,resta,dividir,multiplicacion)" +
                    " en caso de escribir bien la palabra la vuelves a escribir");
            while ((!operacion.equalsIgnoreCase("suma"))&&
                    (!operacion.equalsIgnoreCase("resta"))&&
                            (!operacion.equalsIgnoreCase("dividir"))&&
                                    (!operacion.equalsIgnoreCase("multiplicacion"))) {
                operacion = in.nextLine();
            }
            //envio la operacion ha realizar
            out.writeUTF(operacion);
            System.out.println("n1");
            int n1=in.nextInt();
            //envio dato de n1 despues de pedirlo por teclado
                out.writeByte(n1);
            System.out.println("n2");
            //envio dato de n2 despues de pedirlo por teclado
            int n2=in.nextInt();
                 out.writeByte(n2);

                //imprimo por consola el dato recibido del server
            int total=input.readInt();
            System.out.println(total);
                sc.close();

            //cierro la conexion

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (BindException e3){
            System.out.println("la direccion ya esta en uso");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
