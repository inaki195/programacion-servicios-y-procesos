package Ej1.e5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    public static void main(String[] args) {
        ServerSocket servidor=null;
        Socket sc=null;
        //cliente al servidor manda datos
        DataInputStream in;
        //servidor al cliente manda datos
        DataOutputStream out = null;
        try {
            //acepto conexion (se queda esperando hasta recibir una noticia)
            servidor=new ServerSocket(5005);

                //acepto solicitud
                sc = servidor.accept();

                out=new DataOutputStream(sc.getOutputStream());
                in = new DataInputStream(sc.getInputStream());
                //n1 n2 numeros que se reciben desde el servidor
                    //leo los 2 numeros recibidos mas la operacion
                 String operacion=in.readUTF();
            System.out.println(operacion);
                int n1 = in.read();
                int n2 = in.read();
                System.out.println(" datos recibidos "+n1+" "+n2);


            int ntotal=0;
            //despues hacemos la operacion dependiendo de que dato hemos recibido hacemos
            //una operacion u otra
               switch (operacion){
                   case "suma": {
                       ntotal=n1+n2;
                       System.out.println(ntotal);
                   }
                       break;
                   case "resta": {
                       ntotal=n1-n2;
                       System.out.println(ntotal);
                   }
                   break;
                   case "multiplicar": {
                       ntotal=n1*n2;
                       System.out.println(ntotal);
                   }
                   break;
                   case "dividir": {
                       ntotal=n1/n2;
                       System.out.println(ntotal);
                   }
                   break;
               }
            //envio el resultado de la operacion
            out.writeInt(ntotal);
                //cierro conexion
                sc.close();



        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
