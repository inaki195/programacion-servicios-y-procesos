package Ej1.e7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.BindException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * el cliente 2 recibira el resultado
 */
public class Cliente2 {
    Socket sc;
    public static void main(String[] args) {
        //host
        final String host = "127.0.0.1";
        //puerto  que elijo
        final int puerto = 5005;
        try {
            String operacion="";
            //declaro objeto scanner para coger datos por teclado
            Scanner in=new Scanner(System.in);
                Socket sc = new Socket(host, puerto);
                DataInputStream input = new DataInputStream(sc.getInputStream());
                //manda datos del cliente al servidor
                DataOutputStream out = new DataOutputStream(sc.getOutputStream());
                //imprimo por consola
            System.out.println("numero a escoger");
            int n2=in.nextInt();
            out.writeByte(n2);

            System.out.println(input.readInt());
                sc.close();

            //cierro la conexion

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (BindException e3){
            System.out.println("la direccion ya esta en uso");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
