package Ej2;

import Ej2.mvc.Controlador;
import Ej2.mvc.Modelo;
import Ej2.mvc.Vista;

/**
 * ejecuta el programa con interfaz grafico
 *  antes tendras que ejecutar  la clase {@link Ej2.servidor.Servidor}
 */
public class Main {
    public static void main(String[] args) {
        Vista vista=new Vista();
        Modelo modelo=new Modelo();
        Controlador controlador=new Controlador(vista,modelo);
    }
}
