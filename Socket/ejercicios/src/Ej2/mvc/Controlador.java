package Ej2.mvc;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * clase que comunica los visual con lo logico
 */
public class Controlador implements ActionListener, WindowListener , KeyListener {
    Vista vista;
     Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        meterListener(this);
    }

    /**
     * metodos que add escucha a los botones
     * @param listener
     */
    private void meterListener(ActionListener listener) {
        //panel 1 faltan los operadores
        vista.btnAReset.addActionListener(listener);
        vista.btnA1.addActionListener(listener);
        vista.btnA2.addActionListener(listener);
        vista.btnA3.addActionListener(listener);
        vista.btnA4.addActionListener(listener);
        vista.btnA5.addActionListener(listener);
        vista.btnA6.addActionListener(listener);
        vista.btnA7.addActionListener(listener);
        vista.btnA8.addActionListener(listener);
        vista.btnA9.addActionListener(listener);
        vista.btnA00.addActionListener(listener);
        vista.btnAsumar.addActionListener(listener);
        vista.btnAresta.addActionListener(listener);
        vista.btnAMulti.addActionListener(listener);
        vista.btnAdiv.addActionListener(listener);
        vista.btnAEquals.addActionListener(listener);
        vista.btnAReset.addActionListener(listener);
        vista.btnAPontencia.addActionListener(listener);
        vista.BtnAraiz.addActionListener(listener);
        //panel 2
        vista.btnBreset.addActionListener(listener);
        vista.btnB0.addActionListener(listener);
        vista.btnB1.addActionListener(listener);
        vista.btnB2.addActionListener(listener);
        vista.btnB3.addActionListener(listener);
        vista.btnB4.addActionListener(listener);
        vista.btnB5.addActionListener(listener);
        vista.btnB6.addActionListener(listener);
        vista.btnB7.addActionListener(listener);
        vista.btnB8.addActionListener(listener);
        vista.btnB9.addActionListener(listener);
        vista.btnBequals.addActionListener(listener);
    }

    @Override
    /**
     * depende de que boton toques realizaremos ciertas acciones
     */
    public void actionPerformed(ActionEvent e) {
        String anterior=vista.txtresultado1.getText();
        String anterior2=vista.txtresultado2.getText();
        String comando=e.getActionCommand();

        switch (comando){
            case "Apontecia":{
                boolean valido;
                String numero=vista.txtresultado1.getText();

                valido=comprobarDatos(numero);

                if (valido) {

                    modelo.eviarDatosAlServerCliente1(numero, "potencia");
                }if(!valido){


                }
            }
            break;
            case "Araiz":{
                boolean valido;
                String numero=vista.txtresultado1.getText();

                valido=comprobarDatos(numero);

                if (valido) {
                    JOptionPane.showMessageDialog(null, "en cliente 1º le pasa el radical el cliente 2º le pasa lo enesimo",
                            "advertencia", JOptionPane.WARNING_MESSAGE);
                    modelo.eviarDatosAlServerCliente1(numero, "raiz");
                }if(!valido){


                }
            }
            break;
            case "B=":{
                boolean valido;
                String numero2=vista.txtresultado2.getText();
                valido=comprobarDatos(numero2);
                modelo.eviarDatosAlServerCliente2(numero2);
                if (valido) {
                    modelo.eviarDatosAlServerCliente2(numero2);
                }if(!valido){
                }
               float resultado= modelo.obtenerResultados();
                vista.txtresultado1.setText(String.valueOf(resultado));
                vista.txtresultado2.setText(String.valueOf(resultado));
            }
            break;
            case "SumaA":{
                boolean valido;
                String numero=vista.txtresultado1.getText();

                valido=comprobarDatos(numero);

                if (valido) {

                    modelo.eviarDatosAlServerCliente1(numero, "suma");
                }if(!valido){


                }

            }
            break;
            case "A-":{
                boolean valido;
                String numero=vista.txtresultado1.getText();

                valido=comprobarDatos(numero);

                if (valido) {
                    modelo.eviarDatosAlServerCliente1(numero, "resta");
                }if(!valido){


                }

            }
            break;
            case "Ax":{
                boolean valido;
                String numero=vista.txtresultado1.getText();

                valido=comprobarDatos(numero);

                if (valido) {
                    modelo.eviarDatosAlServerCliente1(numero, "multiplicar");
                }if(!valido){


                }

            }
            break;
            case "A/":{
                boolean valido;
                String numero=vista.txtresultado1.getText();

                valido=comprobarDatos(numero);

                if (valido) {
                    modelo.eviarDatosAlServerCliente1(numero, "dividir");
                }if(!valido){


                }

            }
            break;
            case "B0":{
                vista.txtresultado1.setText(anterior2+"0");
            }
            break;
            case "B1":{
                vista.txtresultado2.setText(anterior2+"1");
            }
            break;
            case "B2":{
                vista.txtresultado2.setText(anterior2+"2");
            }
            break;
            case "B3":{
                vista.txtresultado2.setText(anterior2+"3");
            }
            break;
            case "B44":{
                vista.txtresultado2.setText(anterior2+"4");
            }
            break;
            case "B5":{
                vista.txtresultado2.setText(anterior2+"5");
            }
            break;
            case "B6":{
                vista.txtresultado2.setText(anterior2+"6");
            }
            break;
            case "B7":{
                vista.txtresultado2.setText(anterior2+"7");
            }
            break;
            case "B8":{
                vista.txtresultado2.setText(anterior2+"8");
            }
            break;
            case "B9":{
                vista.txtresultado2.setText(anterior2+"9");
            }
            break;
            case "resetA":{
                vista.txtresultado1.setText("");
            }
            break;
            case "resetB":{
              vista.txtresultado2.setText("");
            }
            break;
            case "0A":{
                vista.txtresultado1.setText(anterior+"0");
            }
            break;
            case "A1":{
                vista.txtresultado1.setText(anterior+"1");
            }
            break;
            case "A2":{
                vista.txtresultado1.setText(anterior+"2");
            }
            break;
            case "A3":{
                vista.txtresultado1.setText(anterior+"3");
            }
            break;
            case "A4":{
                vista.txtresultado1.setText(anterior+"4");

            }
            break;
            case "A5":{
                vista.txtresultado1.setText(anterior+"5");
            }
            break;
            case "A6":{
                vista.txtresultado1.setText(anterior+"6");
            }
            break;
            case "A7":{
                vista.txtresultado1.setText(anterior+"7");
            }
            break;
            case "A8":{
                vista.txtresultado1.setText(anterior+"8");
            }
            break;
            case "A9":{
                vista.txtresultado1.setText(anterior+"9");
            }
            break;


        }
    }

    /**
     * comprueba que el valor sea valido en
     * caso contrario  sale un  msg de error
     * @param numero
     * @return
     */
    private boolean comprobarDatos(String numero) {
        boolean verifacdo = false;

            try {

                float num = Float.parseFloat(numero);
            } catch (NumberFormatException e3) {
                // JOptionPane.showMessageDialog(null,"dato no vaido","error",JOptionPane.ERROR_MESSAGE);
                String num = JOptionPane.showInputDialog("pon un dato valido solo numeros", JOptionPane.OK_CANCEL_OPTION);
                vista.txtresultado1.setText(num);
                verifacdo = false;
            }
            if (numero.equalsIgnoreCase("")) {
                JOptionPane.showMessageDialog(null, "el campo de los numeros esta vacio", "advertencia", JOptionPane.WARNING_MESSAGE);
                return false;
            } else {
                verifacdo = true;
                return true;
            }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
