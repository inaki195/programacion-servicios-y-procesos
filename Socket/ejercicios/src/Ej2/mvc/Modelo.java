package Ej2.mvc;


import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * clase que se encarga de lo logico
 * aqui estaran los 2 clientes
 */
public class Modelo {
    Socket sc;
    Socket sc2;
    DataInputStream inc1;
    DataOutputStream outc1;
    DataInputStream inc2;
    DataOutputStream outc2;
    //server

    private  final String host = "127.0.0.1";
    private  final int puerto = 5005;
    public Modelo() {
        try {
            //inicializacion de servidor

            //inicializamos socket(clientes)
            try {
                sc=new Socket(host,puerto);
                sc2=new Socket(host,puerto);
                outc1=new DataOutputStream(sc.getOutputStream());
                inc1 =new DataInputStream(sc.getInputStream());
                outc2=new DataOutputStream(sc2.getOutputStream());
                inc2 =new DataInputStream(sc2.getInputStream());
            }catch (ConnectException e3){
                JOptionPane.showMessageDialog(null,"el  servidor no esta activado ejecuta 1º la clase del servidor",
                        "error de servidor",JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }




    /**
     * coge los datos que estan en la vista y los envia al servidor
     * @param numero
     * @param operacion
     */
    public void eviarDatosAlServerCliente1(String numero, String operacion) {

        try {
            //envio en n1 y la operacion al servidor
            outc1.writeUTF(numero);
            outc1.writeUTF(operacion);
        } catch (IOException e) {
            e.printStackTrace();
        }
       
    }

    /**
     * datos que envia el cliente 2
     * @param numero2 n2 que enviar al servidor
     */
    public void eviarDatosAlServerCliente2(String numero2) {
        try {
            outc2.writeUTF(numero2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo que recibe el resultado total
     * @return  float resultado enviado del servidor
     */
    public float obtenerResultados() {
        float resultado=0;
        float resultado2=0;
        try {
            //espero los resultado del servidor
             resultado = inc1.readFloat();
             resultado2 = inc2.readFloat();
        }catch (SocketException e3){
            JOptionPane.showMessageDialog(null, "el servido no esta disponible", "servidor apagado", JOptionPane.WARNING_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //al ser el mismo numero que leeomos tanto de socket 1 como del 2 da igual que dato retornar
        return  resultado;
    }
}
