package Ej2.servidor;

public class Prueba {
    /**
     * esta clase es ajena  al programa era para comprobar que funcionaba la raiz enesima
     * @param args
     */
    public static void main(String[] args) {
            double numero = 25;
            double raizCuadrada = Math.sqrt(numero);

            double otroNumero = 125;
            double raizCubica = Math.cbrt(otroNumero);

            double miNumero = 32;
            double raizCubicaConCbr = Math.pow(miNumero, (double) 1 / 5);
        System.out.println("Raiz de "+miNumero+" es "+raizCubicaConCbr);
        }

    }

