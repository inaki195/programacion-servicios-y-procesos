package Ej2.servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Servidor {
    /**
     * metodo principal
     * activa el servidor
     * @param args
     */
    public static void main(String[] args) {
        ServerSocket servidor=null;
        Socket sc=null;
        Socket sc2=null;
        //cliente al servidor manda datos
        DataInputStream in;
        DataInputStream in2;
        //servidor al cliente manda datos
        DataOutputStream out = null;
        DataOutputStream out2 = null;
        try {
            //acepto conexion (se queda esperando hasta recibir una noticia)
            servidor=new ServerSocket(5005);
                //acepto solicitud
                sc = servidor.accept();
                sc2=servidor.accept();
                //establezco conexiones con diferentes socket
                out2=new DataOutputStream(sc2.getOutputStream());
                out=new DataOutputStream(sc.getOutputStream());
                in = new DataInputStream(sc.getInputStream());
                in2=new DataInputStream(sc2.getInputStream());
                //n1 n2 numeros que se reciben desde el servidor
                 String num1=in.readUTF();
            System.out.println("primer numero "+num1);
                String operacion=in.readUTF();
            System.out.println("operacion escogida :" +operacion);
            float n1= Integer.parseInt(num1);

            String dato2 = in2.readUTF();
            System.out.println("segundo numero: "+dato2);
            float n2=Float.parseFloat(dato2);
            float ntotal=0;
            //depende de que dato reciba realizara una operacion o otra
               switch (operacion){
                   case "suma": {
                       ntotal=n1+n2;
                       System.out.println(ntotal);
                        out.writeFloat(ntotal);
                        out2.writeFloat(ntotal);
                   }
                       break;
                   case "resta": {
                       ntotal=n1-n2;
                       System.out.println(ntotal);
                   }
                   break;
                   case "multiplicar": {
                       ntotal=(n1*n2);
                       System.out.println(ntotal);
                   }
                   break;
                   case "dividir": {
                       ntotal=n1/n2;
                       System.out.println(ntotal);
                   }
                   break;
                   case "potencia": {
                       ntotal= (float) Math.pow(n1,n2);
                       System.out.println(ntotal);
                   }
                   break;
                   case "raiz": {
                       ntotal= (float) Math.pow(n1, (double) 1 / n2);
                       System.out.println(ntotal);
                   }
                   break;

               }
            //envio el resultado de la operacion
            out.writeFloat(ntotal);
            out2.writeFloat(ntotal);
               servidor.close();
                //cierro conexion



            //controlo excepciones
        } catch (SocketException r){
            System.out.println("perdida la conexion cliente");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
