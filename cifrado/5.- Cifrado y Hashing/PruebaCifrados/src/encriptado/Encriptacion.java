package encriptado;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Encriptacion {
     MessageDigest md=null;

    /**
     * hashea un mensaje
     * @param key msg
     * @return msg encriptado
     */
    public static String hashsear(String key){
        MessageDigest md=null;
    String has = "";
    byte[] mb = md.digest();
    try {
        md = MessageDigest.getInstance("SHA-1");
        md.update(key.getBytes());
        //Se realiza el Hashing
        mb = md.digest();
        //Se muestra por pantalla
      has= String.valueOf((Hex.encodeHex(mb)));
      
    } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
    }
    return has;
}
    /**
     *
     * @param secretKey clave secreta
     * @param cadena palabra que encriptaremos
     * @return palabra encriptada
     */
    public static String encriptado(String secretKey, String cadena){
        String encriptacion = "";
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            //comprobamos que la llave secreta  para proceder al encriptamiento
            byte[] llavePassword = md5.digest(secretKey.getBytes("utf-8"));
            byte[] BytesKey = Arrays.copyOf(llavePassword, 24);
            SecretKey key = new SecretKeySpec(BytesKey, "DESede");
            Cipher cifrado = Cipher.getInstance("DESede");
            cifrado.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainTextBytes = cadena.getBytes("utf-8");
            byte[] buf = cifrado.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            encriptacion = new String(base64Bytes);
        } catch (Exception ex) {
            System.out.println("algo salio mal");
        }
        return encriptacion;
    }
    /**
     *
     * @param secretKey palabra secreta
     * @param cadenaEncriptada  palabra encriptada
     * @return palabra desencriptada
     */
    public static String desencriptacion(String secretKey , String cadenaEncriptada){
        String desencriptacion = "";
        try {
            byte[] message = Base64.decodeBase64(cadenaEncriptada.getBytes("utf-8"));
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md5.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher decipher = Cipher.getInstance("DESede");
            decipher.init(Cipher.DECRYPT_MODE, key);
            byte[] plainText = decipher.doFinal(message);
            desencriptacion = new String(plainText, "UTF-8");

        } catch (Exception ex) {
            System.out.println("algo salio mal");
        }
        return desencriptacion;
    }
}
