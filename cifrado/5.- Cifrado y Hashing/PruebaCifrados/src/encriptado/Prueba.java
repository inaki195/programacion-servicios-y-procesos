package encriptado;

import sun.reflect.generics.tree.ByteSignature;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;

public class Prueba {
    public static void main(String[] args) {
        String secretKey = "SomosProgramadores";
        Scanner in=new Scanner(System.in);
        System.out.println("Ingresa la cadena a encriptar");
        String cadenaAEncriptar = in.nextLine();
        //encriptamos cadena
        String cadenaEncriptada = encriptado(secretKey, cadenaAEncriptar);
        System.out.println("cadena encriptado :"+cadenaEncriptada);
        String cadenaDesencriptada = desencriptacion(secretKey, cadenaEncriptada);
        System.out.println("cadena desencriptada :"+cadenaDesencriptada);

    }

    /**
     *
     * @param secretKey clave secreta
     * @param cadena palabra que encriptaremos
     * @return palabra encriptada
     */
    public static String encriptado(String secretKey, String cadena){
        String encriptacion = "";
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            //comprobamos que la llave secreta  para proceder al encriptamiento
            byte[] llavePassword = md5.digest(secretKey.getBytes("utf-8"));
            byte[] BytesKey = Arrays.copyOf(llavePassword, 24);
            SecretKey key = new SecretKeySpec(BytesKey, "DESede");
            Cipher cifrado = Cipher.getInstance("DESede");
            cifrado.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainTextBytes = cadena.getBytes("utf-8");
            byte[] buf = cifrado.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            encriptacion = new String(base64Bytes);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Algo salió mal");
        }
        return encriptacion;
    }

    /**
     *
     * @param secretKey palabra secreta
     * @param cadenaEncriptada  palabra encriptada
     * @return palabra desencriptada
     */
    public static String desencriptacion(String secretKey , String cadenaEncriptada){
        String desencriptacion = "";
        try {
            byte[] message = Base64.decodeBase64(cadenaEncriptada.getBytes("utf-8"));
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md5.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher decipher = Cipher.getInstance("DESede");
            decipher.init(Cipher.DECRYPT_MODE, key);
            byte[] plainText = decipher.doFinal(message);
            desencriptacion = new String(plainText, "UTF-8");

        } catch (Exception ex) {
            System.out.println("algo salio mal");
        }
        return desencriptacion;
    }
}
