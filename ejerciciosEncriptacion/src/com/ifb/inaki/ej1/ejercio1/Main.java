package com.ifb.inaki.ej1.ejercio1;


import com.ifb.inaki.ej1.ejercio1.mvc.Controlador;
import com.ifb.inaki.ej1.ejercio1.mvc.Modelo;
import com.ifb.inaki.ej1.ejercio1.mvc.Vista;


/**
 * ejecuta el programa con interfaz grafico
 *  antes tendras que ejecutar  la clase
 */
public class Main {
    public static void main(String[] args) {
        Vista vista=new Vista();
        Modelo modelo=new Modelo();
        Controlador controlador=new Controlador(vista,modelo);
    }
}
