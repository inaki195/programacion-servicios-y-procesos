package com.ifb.inaki.ej1.ejercio1.mvc;



import com.google.gson.Gson;
import com.ifb.inaki.dato.Datos;
import encriptado.Encriptacion;

import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;

/**
 * clase que se encarga de lo logico
 * aqui estaran los 2 clientes
 */
public class Modelo {
    Socket sc;
    Socket sc2;
    DataInputStream inc1;
    DataOutputStream outc1;
    DataInputStream inc2;
    DataOutputStream outc2;
    //server
    Encriptacion ecEncriptacion;

    private  final String host = "127.0.0.1";
    private  final int puerto = 5005;
    public Modelo() {
        ecEncriptacion=new Encriptacion();
        try {
            //inicializacion de servidor
            //establezco conexiones
            //inicializamos socket(clientes)
            try {
                sc=new Socket(host,puerto);
                sc2=new Socket(host,puerto);
                outc1=new DataOutputStream(sc.getOutputStream());
                inc1 =new DataInputStream(sc.getInputStream());
                outc2=new DataOutputStream(sc2.getOutputStream());
                inc2 =new DataInputStream(sc2.getInputStream());
            }catch (ConnectException e3){
                JOptionPane.showMessageDialog(null,"el  servidor no esta activado ejecuta 1º la clase del servidor",
                        "error de servidor",JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }




    /**
     * coge los datos que estan en la vista y los envia al servidor
     * @param numero
     * @param operacion
     */
    public void eviarDatosAlServerCliente1(String numero, String operacion) {
        //objeto Gson
        Gson gson = new Gson();
        try {
            //  creo uno objeto del mi clase creada para posteriormente
            //convertirlo a JSON
            Datos dato=new Datos(numero,operacion);
                String jsonnum=gson.toJson(dato);

            //envio en n1 y la operacion al servidor en un mismo JSON y me ahorro pasar un dato mas( lo mando encriptado)
            outc1.writeUTF(ecEncriptacion.encriptado("secret",jsonnum));

        } catch (IOException e) {
            e.printStackTrace();
        }
       
    }

    /**
     * datos que envia el cliente 2
     * @param numero2 n2 que enviar al servidor
     */
    public void eviarDatosAlServerCliente2(String numero2) {
        //objeto Gson
        Gson gson = new Gson();
        try {
            Datos dato=new Datos(numero2);
            String jsonnum=gson.toJson(dato);
            //mando json encriptado
            outc2.writeUTF(ecEncriptacion.encriptado("secret",jsonnum));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo que recibe el resultado total
     * @return  float resultado enviado del servidor
     */
    public String obtenerResultados() {
        Datos result = null;
        Datos result2 =null;
        Gson gson=new Gson();
        String resultado="";
        String resultado2="";
        String  datoreal="";
        try {
            //espero los resultado del servidor encriptados
             String res = inc1.readUTF();
             resultado2 = inc2.readUTF();
            System.out.println(resultado2);
             //desencriptamos
             res=desencriptar(res);
             resultado2=desencriptar(resultado2);
            System.out.println(resultado2);
            System.out.println(res);
            //lo paso a objeto
             result=gson.fromJson(res,Datos.class);
             result2=gson.fromJson(resultado2,Datos.class);
            System.out.println(result2.getDato());
            System.out.println(result.getDato());
              datoreal=result.getDato();
              //encripto un msg para despues enviarlo al server
              String cierre=ecEncriptacion.encriptado("secret","true");
              outc1.writeUTF(cierre);
        }catch (SocketException e3){
            JOptionPane.showMessageDialog(null, "el servido no esta disponible", "servidor apagado", JOptionPane.WARNING_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //al ser el mismo numero que leeomos tanto de socket 1 como del 2 da igual que dato retornar
        return  datoreal;
    }

    private String desencriptar(String res) {
        res=ecEncriptacion.desencriptacion("secret",res);
        return res;
    }
}
