package com.ifb.inaki.ej2.ejercicio2;

public class Datos {
    private String dato;
    private int numero;

    public Datos(String dato, int numero) {
        this.dato = dato;
        this.numero = numero;
    }
    public String datoso(){
        return  this.dato+" n "+this.numero;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
