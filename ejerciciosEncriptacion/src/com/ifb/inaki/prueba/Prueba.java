package com.ifb.inaki.prueba;

import encriptado.Encriptacion;

public class Prueba {
    public static void main(String[] args) {
        Encriptacion encriptacion=new Encriptacion();
        String palabra="hola";
        System.out.println("palabra original "+palabra);
        String enr=encriptacion.encriptado("secreto",palabra);
        System.out.println("palabra encriptada"+enr);
        String des=encriptacion.desencriptacion("secreto",enr);
        System.out.println("palabra desencriptada "+des);
        System.out.println(encriptacion.hashsear("pepe"));
    }
}
