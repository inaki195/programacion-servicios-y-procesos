package com.ifb.inaki.ej1;

public class Macarena {
    public static void main(String[] args) {
        /**
         * creo 4 objetos de la clase Hilo de abajo
         * despues ejecuto el primero y en la siguiente linea
         * le pongo un join para que nose ejecuten otros hilos mientras
         * el primer hilo termine de ejecutarse y asi con los demas objetos de hilos
         */
        Hilo hilo1=new Hilo(" Dale a tu cuerpo alegría Macarena ");
        Hilo hilo2=new Hilo(" Que tu cuerpo es pa darle alegría y cosa buena");
        Hilo hilo3=new Hilo("  Dale a tu cuerpo alegría Macarena");
        Hilo hilo4=new Hilo("  Heeeeyyyyy Macarena");

        try {
            //para que las estrofas se ejecuten en orden
            //necesitamos sugerir a los hilos
            // que no se ejecute otro hilo hasta
            // que el otro no acabe de ejucatarse
            hilo1.start();
            hilo1.join();
            hilo2.start();
            hilo2.join();
            hilo3.start();
            hilo3.join();
            hilo4.start();
            hilo4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


}

/**
 * clase encargada de ejecutar el hilo
 */
class Hilo extends Thread{
    String msg;
    public Hilo(String mensaje) {
        msg=mensaje;
    }

    public void run(){
        //imprime el mensaje por consola
        System.out.println(msg);
    }
}
