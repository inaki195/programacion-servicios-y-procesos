package com.ifb.inaki.ej2;

import java.io.*;
import java.util.StringTokenizer;

public class Biblia {
    /**
     * @param args
     */
    public static void main(String[] args) {
        //creo hilos le paso un numero que servira  para coger un caso de switch
         Hilo hilo1=new Hilo(1);
        Hilo hilo2=new Hilo(2);
        Hilo hilo3=new Hilo(3);
        Hilo hilo4=new Hilo(4);
        Hilo hilo5=new Hilo(5);
        //los inicialializo y los pongo en start
        hilo1.start();
        hilo2.start();
        hilo3.start();
        hilo4.start();
        hilo5.start();

    }
    static class Hilo extends Thread{
        int opcion;

        public Hilo(int opcion) {
            this.opcion = opcion;
        }

        public void run(){


                    switch (opcion) {
                        case 1:{
                            //cuenta palabras
                            contarPalabras();

                        }
                        break;
                        case 2:{
                            //metodos que cuenta lineas
                            contarLineas();

                        }
                        break;
                        case 3:{
                            //cuenta las veces que dicen dios
                           int dios= leerFichero("Dios","Dios,","Dios.","Dios;","Dios?","Dios:", "Dios!","!dios","¿dios");
                            System.out.println("veces que dicen Dios "+dios);
                            //getClass().wait();
                        }
                        break;
                        case 4:{
                            //cuenta las veces que dicen Moises
                           // getClass().notifyAll();
                           int moises= leerFichero("Moises","Moises,","Moises.","Moises?","Moises;","Moises:","Moises!", "!dios", "¿dios");
                            System.out.println("veces que dicen Moises "+moises);
                            //getClass().wait();
                        }
                        break;
                        case 5:{
                            //cuenta las veces que dicen Jesus
                            int jesus=leerFichero("Jesus","Jesus,","Jesus.","Jesus?","Jesus;","Jesus:", "Dios!", "!dios", "¿dios");
                            System.out.println("veces que dicen Jesus "+jesus);
                        }
                        break;




            }
        }

        /**
         * metodo que cuentas cuantas veces se repite la palabra leyeendo hasta
         * final de fichero
         * @param b1 palabra
         * @param b2 palabra,
         * @param b3 palabra.
         * @param b4 palabra?
         * @param b5 palabra;
         * @param b6 palabra:
         * @param b7 palabra!
         * @param b8 !palabra
         * @param b9 ¿palabra
         * @return int el numero de palabras
         */
        private int leerFichero(String b1, String b2, String b3, String b4, String b5, String b6, String b7, String b8, String b9) {
        //ahora                2613    483 Moises 652 Jesus
       //actual               4180 Dios 802 Moises 946 Jesus
        //faltan                705      78          117
            //deberia calcular 4834 Dios 880 Moises 1030 Jesus

            int contador=0;
            File fichero = new File("Biblia.txt");
            try {
                BufferedReader archivoLeer=new BufferedReader(new FileReader(fichero));
                String lineaLeida;
                while ((lineaLeida=archivoLeer.readLine())!=null){
                    //contamos linea
                      StringTokenizer st=new StringTokenizer(lineaLeida);
                      String frase= String.valueOf(st);
                      //si coincinde se suma
                    if(((lineaLeida.contains(b1)))){
                        contador++;
                    }
                    if(((lineaLeida.contains(b2)))){
                        contador++;
                    }
                    if((lineaLeida.contains(b3))){
                        contador++;
                    } if((lineaLeida.contains(b4))){
                        contador++;
                    }
                    if((lineaLeida.contains(b5))){
                        contador++;
                    }
                    if((lineaLeida.contains(b6))){
                        contador++;
                    }
                    if((lineaLeida.contains(b7))){
                        contador++;
                    }
                    if((lineaLeida.contains(b8))){
                        contador++;
                    }
                    if((lineaLeida.contains(b9))){
                        contador++;
                    }


                }
                archivoLeer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            return contador;
        }


        /**
         * metodo un que le todo el fichero linea  a linea
         * y va sumando a una variables que cuenta lineeas
         * despues cierro buffer y escribo el numero de lineas resultantes
         */
        private void contarLineas() {
            int contadorLine=0;
            File fichero = new File("Biblia.txt");
            try {
                BufferedReader archivoLeer=new BufferedReader(new FileReader(fichero));
                String lineaLeida;
                while ((lineaLeida=archivoLeer.readLine())!=null){
                    //contamos linea
                    contadorLine=contadorLine+1;
                }
                archivoLeer.close();
                System.out.println("lineas: "+contadorLine);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        /**
         * metodo que  cuenta palabras con un bufferreader
         * y leemos hasta final de fichero
         * cogemos una linea y contamos las palabras .conutTokens
         * cerramos en buffer y escribimos el numero de palabras
         */
        private void contarPalabras() {

            int contadorword=0;

            File fichero = new File("Biblia.txt");
            try {
                BufferedReader archivoLeer=new BufferedReader(new FileReader(fichero));
                String lineaLeida;
                while ((lineaLeida=archivoLeer.readLine())!=null){
                    //contamos de palabras
                    StringTokenizer st=new StringTokenizer(lineaLeida);
                    contadorword=contadorword+st.countTokens();
                }
                archivoLeer.close();
                System.out.println("\n"+"numero de palabras: "+contadorword);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
