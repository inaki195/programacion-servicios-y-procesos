package com.ifb.inaki.ej3;

import java.io.*;
import java.util.Properties;

public class SumaMatriz {
    public static void main(String[] args) {
    double matriz_alfa [][]= new double[3][3];
    double matriz_omega [][]= new double[3][3];
        double matriz_resultante [][]= new double[3][3];
        double m_resultado [][]= new double[3][3];
       matriz_alfa= rellenarPrimeraMatriz(matriz_alfa);
       matriz_omega=rellenarSegundaMatriz(matriz_omega);
       dibujarMatrices(matriz_alfa,matriz_omega);
       //a 3 primeros hilos le pasamos las 2 matrices y calculamos las filas
        //fila 1
       Hilo fila1=new Hilo(matriz_alfa,matriz_omega,0);
       //fila 2
       Hilo fila2=new Hilo(matriz_alfa,matriz_omega,2);
       //fila 3
       Hilo fila3=new Hilo(matriz_alfa,matriz_omega,3);
       //calcula el resultado
       Hilo fila4=new Hilo(4,m_resultado);
        /**
         * el hilo uno,dos y tres se tienen que ejecutarse antes que el cuarto
         * porque el cuarto hilo muestra el resultado y no puedes mostrar el resultado
         * si  los otros hilos  no han finalizado
         *
         */
        try {
            fila1.start();
            fila1.join();
            fila2.start();
            fila2.join();
            fila3.start();
            fila3.join();
            fila4.start();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        
    }

    /**
     * dibuja las matrices que le pasamos con un bucle for
     * @param matriz_alfa matriz de double
     * @param matriz_omega matriz de double
     */
    private static void dibujarMatrices(double[][] matriz_alfa, double[][] matriz_omega) {

        for (int i=0;i<matriz_alfa.length;i++){

            for (int j=0;j<matriz_alfa.length;j++){
                System.out.print("["+matriz_alfa[i][j]+"] ");

            }
            if (i==0){
                System.out.print("    ");
            }
            if (i==1) {
                System.out.print("    ");
            }
            if (i==2){
                System.out.print("    ");
            }

            System.out.println(" ");
        }
        System.out.println("---------------------------------------------------------");
        for (int i=0;i<matriz_omega.length;i++) {
            for (int j = 0; j < matriz_omega.length; j++) {
                System.out.print(" [" + matriz_omega[i][j] + "] ");
            }
            System.out.println();
        }


    }
    /**
     * llenado de la primera matriz con numeros aleatorios
     * @param matriz_alfa vacia
     * @return matriz_alfa llena
     */
    private static double[][] rellenarPrimeraMatriz(double[][] matriz_alfa) {
        for (int i=0;i<matriz_alfa.length;i++){
            for (int j=0;j<matriz_alfa.length;j++){
               matriz_alfa[i][j]=Math.random()*5;
            }
        }
        return matriz_alfa;
    }
    /**
     * llenado de la segunda Matriz con numeros aleatorios
     * @param matriz_omega vacia
     * @return matriz_omega llena
     */
    private static double[][] rellenarSegundaMatriz(double[][] matriz_omega) {
        for (int i=0;i<matriz_omega.length;i++){
            for (int j=0;j<matriz_omega.length;j++){
                matriz_omega[i][j]=Math.random()*5;
            }
        }
        return matriz_omega;
    }


    static class Hilo extends Thread{
        int fila;
        double matriz_alfa[][];
        double matriz_omega[][];
        double m_resultado[][];

        public Hilo(double[][] matriz_alfa, double[][] matriz_omega, int fila) {
            this.fila=fila;
            this.matriz_alfa=matriz_alfa;
            this.matriz_omega=matriz_omega;
        }

        public Hilo(int fila, double[][] m_resultado) {
            this.fila = fila;
            this.m_resultado = m_resultado;
        }

        public void run(){
        switch (fila){

            case 0:{
                double n1=0;
                double n2=0;
                double n3=0;
                double p1=0;
                double p2=0;
                double p3=0;
                //un fichero por cada linea de matriz
                //las envio a un fichero para que se graben con un fichero .conf
                //de esta forma puedo guardar los resultados despues de la ejecucion
                //en caso de ejucarse  se sobrescriben los datos por lo tanto no seran ambiguos
                //guardo los resultado de la primera fila y de ambas matrices y las sumo
                for (int i=0;i<matriz_omega.length;i++){
                    n1=matriz_alfa[0][0];
                    n2=matriz_alfa[0][1];
                    n3=matriz_alfa[0][2];
                }
                System.out.println("");
                for (int i=0;i<matriz_omega.length;i++){
                    p1=matriz_omega[0][0];
                    p2=matriz_omega[0][1];
                    p3=matriz_omega[0][2];
                }
                System.out.println("");
                double a1=p1+n1;
                double a2=p2+n2;
                double a3=p3+n3;
                escribirEnfichero(a1,a2,a3);
            }
            break;
            case 2:{
                double n1=0;
                double n2=0;
                double n3=0;
                double p1=0;
                double p2=0;
                double p3=0;
                for (int i=0;i<matriz_omega.length;i++){
                    n1=matriz_alfa[1][0];
                    n2=matriz_alfa[1][1];
                    n3=matriz_alfa[1][2];
                }
                for (int i=0;i<matriz_omega.length;i++){
                    p1=matriz_omega[1][0];
                    p2=matriz_omega[1][1];
                    p3=matriz_omega[1][2];
                }
                System.out.println("");
                //sumo variables de diferentes matrices pero mismas posiciones
                //para hacer la suma
                double a1=p1+n1;
                double a2=p2+n2;
                double a3=p3+n3;
                //las envio a un fichero para que se graben con un fichero .conf
                //de esta forma puedo guardar los resultados despues de la ejecucion
                //en caso de ejucarse  se sobrescriben los datos por lo tanto no seran ambiguos
                escribirEnfichero2(a1,a2,a3);
            }
            break;
            case 3:{
                double n1=0;
                double n2=0;
                double n3=0;
                double p1=0;
                double p2=0;
                double p3=0;

                for (int i=0;i<matriz_omega.length;i++){
                    n1=matriz_alfa[2][0];
                    n2=matriz_alfa[2][1];
                    n3=matriz_alfa[2][2];

                }
                System.out.println("");
                for (int i=0;i<matriz_omega.length;i++){
                    p1=matriz_omega[2][0];
                    p2=matriz_omega[2][1];
                    p3=matriz_omega[2][2];
                }
                System.out.println("");
                //un fichero por cada linea de matriz
                //las envio a un fichero para que se graben con un fichero .conf
                //de esta forma puedo guardar los resultados despues de la ejecucion
                //en caso de ejucarse  se sobrescriben los datos por lo tanto no seran ambiguos
                double a1=p1+n1;
                double a2=p2+n2;
                double a3=p3+n3;
                escribirEnfichero3(a1,a2,a3);
            }
            break;
            case 4:{
                //mandamos la matriz a un metodo para que la rellenamos
                //para despues imprimirla por pantalla
                System.out.println("***RESULTADO***");
                m_resultado=leerLineas(m_resultado);
                for (int i=0;i< m_resultado.length;i++){
                    for (int j=0;j< m_resultado.length;j++){
                        System.out.print("["+m_resultado[i][j]+"]");
                    }
                    System.out.println();
                }
            }
            break;
        }
        }

        /**
         * metodo que lee de fichero  .conf y lo que lee lo manda
         * a una variable double y se la asigna a la correspondiente posicion de la matriz
         * @param m_resultado matriz vacia
         * @return m_resultado matriz que vuelve llena con las sumas ya hechas
         * */
        private double[][] leerLineas(double[][] m_resultado) {
            Properties configuracion = new Properties();
            try {
                configuracion.load(new FileInputStream("linea1.conf"));
                configuracion.load(new FileInputStream("linea2.conf"));
                configuracion.load(new FileInputStream("linea3.conf"));
               double a1 = Double.parseDouble(configuracion.getProperty("a1"));
                double a2 = Double.parseDouble(configuracion.getProperty("a2"));
                double a3 = Double.parseDouble(configuracion.getProperty("a3"));
                double b1= Double.parseDouble(configuracion.getProperty("b1"));
                double b2= Double.parseDouble(configuracion.getProperty("b2"));
                double b3=Double.parseDouble(configuracion.getProperty("b3"));
                double c1= Double.parseDouble(configuracion.getProperty("c1"));
                double c2= Double.parseDouble(configuracion.getProperty("c2"));
                double c3= Double.parseDouble(configuracion.getProperty("c3"));
                m_resultado[0][0]=a1;
                m_resultado[0][1]=a2;
                m_resultado[0][2]=a3;
                m_resultado[1][0]=b1;
                m_resultado[1][1]=b2;
                m_resultado[1][2]=b3;
                m_resultado[2][0]=c1;
                m_resultado[2][1]=c2;
                m_resultado[2][2]=c3;
            } catch (FileNotFoundException fnfe ) {
                fnfe.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            return m_resultado;
        }

        /**
         * como entre ejecucion de un hilo y otro no se guardan los resultados escribo en
         * un fichero los resultados de las sumas de las lineas para guardarlas
         * @param a1 resultado de la suma de matrices tercera fila
         * @param a2 resultado de la suma de matrices tercera fila
         * @param a3 resultado de la suma de matrices tercera fila
         */
        private void escribirEnfichero3(double a1, double a2, double a3) {
            Properties configuracion = new Properties();
            configuracion.setProperty("c1", String.valueOf(a1));
            configuracion.setProperty("c2", String.valueOf(a2));
            configuracion.setProperty("c3", String.valueOf(a3));
            try {
                configuracion.store(new FileOutputStream("linea3.conf"),
                        "suma de matriz primera linea");
            } catch (FileNotFoundException fnfe ) {
                fnfe.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

        }
        /**
         * como entre ejecucion de un hilo y otro no se guardan los resultados escribo en
         * un fichero los resultados de las sumas de las lineas para guardarlas
         * @param a1 resultado de la suma de matrices segunda linea
         * @param a2 resultado de la suma de matrices segunda linea
         * @param a3 resultado de la suma de matrices segunda linea
         */
        private void escribirEnfichero2(double a1, double a2, double a3) {
            Properties configuracion = new Properties();
            configuracion.setProperty("b1", String.valueOf(a1));
            configuracion.setProperty("b2", String.valueOf(a2));
            configuracion.setProperty("b3", String.valueOf(a3));
            try {
                configuracion.store(new FileOutputStream("linea2.conf"),
                        "suma de matriz segunda linea");
            } catch (FileNotFoundException fnfe ) {
                fnfe.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        /**
         * como entre ejecucion de un hilo y otro no se guardan los resultados escribo en
         * un fichero los resultados de las sumas de las lineas para guardarlas
         * @param a1 resultado de la suma de matrices primera linea
         * @param a2 resultado de la suma de matrices primera linea
         * @param a3
         */
        private void escribirEnfichero(double a1, double a2, double a3) {
            Properties configuracion = new Properties();
            configuracion.setProperty("a1", String.valueOf(a1));
            configuracion.setProperty("a2", String.valueOf(a2));
            configuracion.setProperty("a3", String.valueOf(a3));
            try {
                configuracion.store(new FileOutputStream("linea1.conf"),
                        "suma de matriz primera linea");
            } catch (FileNotFoundException fnfe ) {
                fnfe.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

        }
    }
}
