package com.ifb.inaki.ej4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Ecuaciones {
    double resultado1;
    double resultado2;

    /**
     * lo primero que se crean los hilos sin inicializar
     * después se entrara en un bucle que se rompera cuando los 3 numeros
     * que pidamos por teclado esten entre 0 y 10
     * despues se inicializaran hilos pasandole por parametros los numeros
     * introducidos con anterioridad
     * se dara prioridad al primer y segundo hilo cuando terminen se ejecutara el 3 hilo
     * @param args
     */
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        boolean apto=true;
        //creo hilos
        Hilo hilo1;
        Hilo hilo2;
        Hilo hilo3;
        //creo variables
        int x = 0;
        int y = 0;
        int z = 0;

        // pido 2 valores para los numeros cuando ambos
        //esten correctos se saldra del bucle
        while (apto){
            System.out.println("valor que le damos a X");
             x=scanner.nextInt();
            System.out.println("valor que le damos a Y");
             y=scanner.nextInt();
            System.out.println("valor que le damos a Z");
             z=scanner.nextInt();
            if ((x<10 && x>0) &&(x<10 && x>0)&&(z<10 && z>0)){
                apto=false;
            //en caso que ponga un valor no correspondiente
            }else{
                System.out.println("algo pusiste mal los valores tienen que ser" +
                        "entre 1 y 10 ");
            }
        }

        hilo1=new Hilo(x,y,z,1);
        hilo2=new Hilo(x,y,z,2);
        hilo3=new Hilo(x,y,z,3);
        hilo1.start();
        hilo2.start();
        try {
            hilo1.join();
            hilo2.join();
            hilo3.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
class Hilo extends Thread{
    double x;
    double y;
    double z;
    int i;
    double resultadoTotal;
    double resultado=0;
    double resultado2=0;

    public Hilo(double x, double y, int z, int i) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.i = i;
    }

    public  void run(){
     synchronized (getClass()){
         switch (i) {
             case 1:{
                 try {

                     sleep(2000);
                     System.out.println("resultado del 1º hilo "+  calcularResultado1(x,y,z));
                     resultado=calcularResultado1(x,y,z);
                     escribirEnFichero(resultado);
                     sleep(2000);
                 } catch (InterruptedException e) {
                     e.printStackTrace();
                 }
             }
             break;
             case 2:{
                 try {
                     sleep(2000);
                     double nump=(5*z)-(7*y)+(Math.pow(x,2));
                     double res1=Math.pow(nump,3);
                     double nump2=(4*x)+(3*y)+(8*z);
                     double res2=Math.pow(nump2,2);
                     resultado2=res1-res2;
                     escribirEnFichero2(resultado2);
                     sleep(2000);
                     System.out.println("resultado del 2º hilo "+resultado2);
                 } catch (InterruptedException e) {
                     e.printStackTrace();
                 }
             }
             break;
             case 3:{
                 //lee de fichero  obteniendo resultados de la primera y segunda ecuacion
                 double n1=obtenerResultado1();
                 double n2=obtenerResultado2();
                 System.out.println( n1+" "+n2);
                 double resultadoFinal=n1+n2;
                 System.out.println("el resultado final es "+resultadoFinal);
             }
             break;
         }
     }
    }

    /**
     * escribe en un fichero el resultado de la 2º ecuacion
     * @param  resultado2 de la 2º ecuacion
     */
    private void escribirEnFichero2(double resultado2) {

        PrintWriter escritor=null;
        try {
            escritor=new PrintWriter("archivo2.txt");
            escritor.println(resultado2);
            escritor.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    /**
     * lee de fichero para obtener el resultado en la primera encuacion
     * @return resultado de la 1º ecuacion
     */
    private double obtenerResultado1(){
        File fichero=new File("archivo.txt");
        Scanner lector=null;
        double n1 = 0;
        try {
            lector=new Scanner(fichero);
            while (lector.hasNextLine()){
                n1= Double.parseDouble(lector.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return  n1;

    }

    /**
     * lee de fichero para obtener el resultado en la segunda encuacion
     * @return resultado de la 2º ecuacion
     */
    private double obtenerResultado2() {
        File fichero=new File("archivo2.txt");
        Scanner lector=null;
        double n2 = 0;
        try {
            lector=new Scanner(fichero);
            while (lector.hasNextLine()){
               n2= Double.parseDouble(lector.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return  n2;
    }

    /**
     * escribe en un fichero el resultado de la 1º ecuacion
     * @param resultado
     */
    private void escribirEnFichero(double resultado) {
        PrintWriter escritor=null;
        try {
            escritor=new PrintWriter("archivo.txt");
            escritor.println(resultado);
            escritor.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo que calcula la la operacion , (x+y)2- (4x+3y-2z)
     * @param x
     * @param y
     * @param z
     * @return el resultado de la ecuacion
     */
    private double calcularResultado1(double x, double y, double z) {
        double nm=(x+y);
        double num2=(4*x)+(3*y)-(2*z);
        resultado=(Math.pow(nm,2))-(num2);
        return resultado;
    }
}
