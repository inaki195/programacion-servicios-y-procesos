package com.ifb.inaki.dato;

public class Datos {
    private String dato;
    private String operacion;
    public Datos(String dato) {
        this.dato = dato;
    }

    public Datos(String dato, String operacion) {
        this.dato = dato;
        this.operacion = operacion;
    }

    public String getDato() {
        return dato;
    }



    public String getOperacion() {
        return operacion;
    }
}
