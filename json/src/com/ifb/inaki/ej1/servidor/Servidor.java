package com.ifb.inaki.ej1.servidor;

import com.google.gson.Gson;
import com.ifb.inaki.dato.Datos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Servidor {
    /**
     * metodo principal
     * activa el servidor
     * @param args
     */
    public static void main(String[] args) {
        ServerSocket servidor=null;
        Socket sc=null;
        Socket sc2=null;
        Gson gson = new Gson();
        //cliente al servidor manda datos
        DataInputStream in;
        DataInputStream in2;
        //servidor al cliente manda datos
        DataOutputStream out = null;
        DataOutputStream out2 = null;
        try {
            //acepto conexion (se queda esperando hasta recibir una noticia)
            servidor=new ServerSocket(5005);
                //acepto solicitud
                sc = servidor.accept();
                sc2=servidor.accept();
                //establezco conexiones con diferentes socket
                out2=new DataOutputStream(sc2.getOutputStream());
                out=new DataOutputStream(sc.getOutputStream());
                in = new DataInputStream(sc.getInputStream());
                in2=new DataInputStream(sc2.getInputStream());
                //n1 n2 numeros que se reciben desde el servidor
                //recibo el dato en json
                 String num1=in.readUTF();
            System.out.println("dato original del cliente 1 "+num1);
                 //lo paso a objeto
            Datos numero1=gson.fromJson(num1,Datos.class);
            System.out.println("primer numero "+numero1.getDato());
            System.out.println("operacion "+numero1.getOperacion());

            String operacionReal=numero1.getOperacion();
            //muestro la operacion en formato JSON

            //parseo el objeto a float
            float n1= Integer.parseInt(numero1.getDato());
            //leo el dato JSON
            String dato2 = in2.readUTF();
            System.out.println("dato original del cliente 2 "+dato2);
            //lo paso a un objeto
            Datos numero2=gson.fromJson(dato2,Datos.class);
            System.out.println("segundo numero: "+numero2.getDato());
            //paso String float el segundo numero
            float n2=Float.parseFloat(numero2.getDato());
            float ntotal=0;
            //depende de que dato reciba realizara una operacion o otra
               switch (operacionReal){
                   case "suma": {
                       ntotal=n1+n2;
                       System.out.println(ntotal);
                        out.writeFloat(ntotal);
                        out2.writeFloat(ntotal);
                   }
                       break;
                   case "resta": {
                       ntotal=n1-n2;
                       System.out.println(ntotal);
                   }
                   break;
                   case "multiplicar": {
                       ntotal=(n1*n2);
                       System.out.println(ntotal);
                   }
                   break;
                   case "dividir": {
                       ntotal=n1/n2;
                       System.out.println(ntotal);
                   }
                   break;
                   case "potencia": {
                       ntotal= (float) Math.pow(n1,n2);
                       System.out.println(ntotal);
                   }
                   break;
                   case "raiz": {
                       ntotal= (float) Math.pow(n1, (double) 1 / n2);
                       System.out.println(ntotal);
                   }
                   break;

               }
            //envio el resultado de la operacion
            //cre objeto de la clase resultado
            Datos resultado=new Datos(String.valueOf(ntotal));
               //convierto objeto a JSON
               String envioresultado=gson.toJson(resultado);
            out.writeUTF(envioresultado);
            out2.writeUTF(envioresultado);
               servidor.close();
                //cierro conexion



            //controlo excepciones
        } catch (SocketException r){
            System.out.println("perdida la conexion cliente");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
