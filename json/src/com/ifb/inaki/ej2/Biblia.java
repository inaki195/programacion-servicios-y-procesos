package com.ifb.inaki.ej2;

import com.google.gson.Gson;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * clase servidor
 */
public class Biblia {
    private static ArrayList<Datos> datos;
    static String dato1;
    static Gson gson;

    //cliente al servidor manda datos
    static DataInputStream in;
    //servidor al cliente manda datos
    static DataOutputStream out = null;

    private String PalabraTotales;

    /**
     * @param args
     */
    public static void main(String[] args) {
        gson=new Gson();
        datos=new ArrayList<>();
        String datoRecibido="";
        //socket servidor
        ServerSocket servidor=null;
        //socket por el cual enviadoremos informacion al servidor
        Socket sc=null;
        boolean seguir=true;
        try {
            servidor = new ServerSocket(5005);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (seguir) {
            DatoCliente cliente = null;
            try {


                sc = servidor.accept();
                out = new DataOutputStream(sc.getOutputStream());
                in = new DataInputStream(sc.getInputStream());
                datoRecibido = in.readUTF();
                cliente = gson.fromJson(datoRecibido, DatoCliente.class);
                System.out.println("dato recibido :" + datoRecibido);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //los inicialializo
            Hilo hilo1 = new Hilo(1);
            Hilo hilo2 = new Hilo(2);
            Hilo hilo3 = new Hilo(3);
            Hilo hilo4 = new Hilo(4);
            Hilo hilo5 = new Hilo(5);
            switch (cliente.getDato()) {
                case "1": {
                    hilo1.start();
                }
                break;
                case "2": {
                    hilo2.start();
                }
                break;
                case "3": {
                    hilo3.start();
                }
                break;
                case "4": {
                    hilo4.start();
                }
                break;
                case "5": {
                    hilo5.start();
                }
                break;
                case "6":{
                    seguir=false;
                }
                break;

            }
        }
        try {
            servidor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static Datos obtenerDatos(String s) {
        Datos elegido = null;
        System.out.println(datos.isEmpty());
        for (Datos d:datos){
            System.out.println(d.toString());
            if (d.getDato().equalsIgnoreCase(s)){
                elegido=d;
                System.out.println("encontrado");
            }
        }
        return elegido;
    }


    static class Hilo extends Thread{
        int opcion;


        public Hilo(int opcion) {
            this.opcion = opcion;
        }

        public void run(){


                    switch (opcion) {
                        case 1:{
                            //cuenta palabras
                            contarPalabras();

                        }
                        break;
                        case 2:{
                            //metodos que cuenta lineas
                            contarLineas();

                        }
                        break;
                        case 3:{
                            //cuenta las veces que dicen dios
                           int dios= leerFichero("Dios","Dios,","Dios.","Dios;","Dios?","Dios:", "Dios!","!dios","¿dios");
                            System.out.println("veces que dicen Dios "+dios);
                            //getClass().wait();
                        }
                        break;
                        case 4:{
                            //cuenta las veces que dicen Moises
                           // getClass().notifyAll();
                           int moises= leerFichero("Moises","Moises,","Moises.","Moises?","Moises;","Moises:","Moises!", "!dios", "¿dios");
                            System.out.println("veces que dicen Moises "+moises);
                            //getClass().wait();
                        }
                        break;
                        case 5:{
                            //cuenta las veces que dicen Jesus
                            int jesus=leerFichero("Jesus","Jesus,","Jesus.","Jesus?","Jesus;","Jesus:", "Dios!", "!dios", "¿dios");
                            System.out.println("veces que dicen Jesus "+jesus);
                        }
                        break;




            }
        }

        /**
         * metodo que cuentas cuantas veces se repite la palabra leyeendo hasta
         * final de fichero
         * @param b1 palabra
         * @param b2 palabra,
         * @param b3 palabra.
         * @param b4 palabra?
         * @param b5 palabra;
         * @param b6 palabra:
         * @param b7 palabra!
         * @param b8 !palabra
         * @param b9 ¿palabra
         * @return int el numero de palabras
         */
        private int leerFichero(String b1, String b2, String b3, String b4, String b5, String b6, String b7, String b8, String b9) {
        //ahora                2613    483 Moises 652 Jesus
       //actual               4180 Dios 802 Moises 946 Jesus
        //faltan                705      78          117
            //deberia calcular 4834 Dios 880 Moises 1030 Jesus

            int contador=0;
            File fichero = new File("Biblia.txt");
            try {
                BufferedReader archivoLeer=new BufferedReader(new FileReader(fichero));
                String lineaLeida;
                while ((lineaLeida=archivoLeer.readLine())!=null){
                    //contamos linea
                      StringTokenizer st=new StringTokenizer(lineaLeida);
                      String frase= String.valueOf(st);
                      //si coincinde se suma
                    if(((lineaLeida.contains(b1)))){
                        contador++;
                    }
                    if(((lineaLeida.contains(b2)))){
                        contador++;
                    }
                    if((lineaLeida.contains(b3))){
                        contador++;
                    } if((lineaLeida.contains(b4))){
                        contador++;
                    }
                    if((lineaLeida.contains(b5))){
                        contador++;
                    }
                    if((lineaLeida.contains(b6))){
                        contador++;
                    }
                    if((lineaLeida.contains(b7))){
                        contador++;
                    }
                    if((lineaLeida.contains(b8))){
                        contador++;
                    }
                    if((lineaLeida.contains(b9))){
                        contador++;
                    }


                }
                archivoLeer.close();
                enviarJson(b1, String.valueOf(contador));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            return contador;
        }


        /**
         * metodo un que le todo el fichero linea  a linea
         * y va sumando a una variables que cuenta lineeas
         * despues cierro buffer y escribo el numero de lineas resultantes
         */
        private void contarLineas() {
            int contadorLine=0;
            File fichero = new File("Biblia.txt");
            try {
                BufferedReader archivoLeer=new BufferedReader(new FileReader(fichero));
                String lineaLeida;
                while ((lineaLeida=archivoLeer.readLine())!=null){
                    //contamos linea
                    contadorLine=contadorLine+1;
                }
                archivoLeer.close();
                System.out.println("lineas: "+contadorLine);
                String linea="lineas";
                enviarJson(linea, String.valueOf(contadorLine));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        /**
         * metodo que  cuenta palabras con un bufferreader
         * y leemos hasta final de fichero
         * cogemos una linea y contamos las palabras .conutTokens
         * cerramos en buffer y escribimos el numero de palabras
         */
        private void contarPalabras() {

            int contadorword=0;

            File fichero = new File("Biblia.txt");
            try {
                BufferedReader archivoLeer=new BufferedReader(new FileReader(fichero));
                String lineaLeida;
                while ((lineaLeida=archivoLeer.readLine())!=null){
                    //contamos de palabras
                    StringTokenizer st=new StringTokenizer(lineaLeida);
                    contadorword=contadorword+st.countTokens();
                }
                archivoLeer.close();
                System.out.println("\n"+"numero de palabras: "+contadorword);
                String palabras="numero de palabras: ";
                dato1=palabras;
                String numero= String.valueOf(contadorword);
                enviarJson(palabras,numero);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * envia json al cliente
         * @param palabras
         * @param numero
         */
        private void enviarJson(String palabras, String numero) {
            Datos dato = new Datos(palabras, Integer.parseInt(numero));
            //creo objeto Gson
            Gson gson=new Gson();
            //lo paso a Json
            String json=gson.toJson(dato);
            try {
                //los envio al cliente
                out.writeUTF(json);
            } catch (IOException e) {
                e.printStackTrace();
            }

        
        }


    }




}
