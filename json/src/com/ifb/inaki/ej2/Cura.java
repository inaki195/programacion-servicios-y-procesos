package com.ifb.inaki.ej2;

import com.google.gson.Gson;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * clase cliente
 */
public class Cura {

    //localhost
    final static String host = "127.0.0.1";
    final static int puerto = 5005;
    public static void main(String[] args) {
        //objeto scanner
        Scanner in =new Scanner(System.in);
            boolean validacion=false;
            String opcion="";



        try {
            while (!opcion.equalsIgnoreCase("6")) {
                Socket sc = new Socket(host, puerto);
                //para enviar y recibir datos del servidor
                DataInputStream input = new DataInputStream(sc.getInputStream());
                DataOutputStream out = new DataOutputStream(sc.getOutputStream());
                //pedimo la opcion por teclado
                ArrayList<String> listanum = new ArrayList<>();
                listanum = rellenarArray(listanum);
                opcion = validacionDeDatos(in, validacion, opcion, listanum);
                //creo objeto json;
                Gson gson = new Gson();
                //creo objeto cliente
                DatoCliente cliente = new DatoCliente(comprobacion(opcion), opcion);
                //paso el objeto
                //manda datos del cliente al servidor
                String jsonenvio = gson.toJson(cliente);
                out.writeUTF(jsonenvio);
                String json="";
                if (!opcion.equalsIgnoreCase("6")) {
                     json = input.readUTF();
                    System.out.println("dato recibido :" + json);
                }else{
                    System.out.println("desconectado");
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }







    }

    private static boolean comprobacion(String opcion) {
        if (opcion.equalsIgnoreCase("6")){
            return false;
        }else{
            return true;
        }
    }

    /**
     * comprueba que el dato que se vaya a enviar este entre 1 y 6
     * @param in objeto de la clase scanners
     * @param validacion boolean que rompe bucle
     * @param opciones dato que se
     * @param listanum
     */
    private static String  validacionDeDatos(Scanner in, boolean validacion, String opciones, ArrayList<String> listanum) {
        texto();
         opciones=in.nextLine();

        //los unicos datos validos son 1,2,3,4,5
        while (!validacion){
            if (listanum.contains(opciones)){
                System.out.println("valido ");
                validacion=true;
            }else{
                texto();
                System.out.println("unicos datos validos(1 , 2 , 3, 4 , 5, 6)");
                opciones=in.nextLine();
            }

        }
        return opciones;
    }

    /**
     * muestra texto
     */
    private static void texto(){
        System.out.println("selecciona una opcion "+
                "\n 1.palabras totales del fichero"+
                "\n 2.lineas totales del fichero"+
                "\n 3.cuantas veces dicen la palabra Dios"+
                "\n 4.cuantas veces dicen la palabra Moises"+
                "\n 5.cuantas veces dicen la palabra Jesus"+
                "\n 6.salir");
    }

    /**
     * rellena el array de String
     * @param listanum array vacio
     * @return listanum array con contenido
     */
    private static ArrayList<String> rellenarArray(ArrayList<String> listanum) {
        listanum.add("1");
        listanum.add("2");
        listanum.add("3");
        listanum.add("4");
        listanum.add("5");
        listanum.add("6");
        return listanum;
    }
}
