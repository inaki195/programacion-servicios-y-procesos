package com.ifb.inaki.ej2;

public class DatoCliente {
    private boolean seguimos;
    private String dato;

    public DatoCliente(boolean seguimos, String dato) {
        this.seguimos = seguimos;
        this.dato = dato;
    }

    public boolean isSeguimos() {
        return seguimos;
    }

    public void setSeguimos(boolean seguimos) {
        this.seguimos = seguimos;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }
}
